package com.sr.heybuddyservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeyBuddyService {
	
	@GetMapping(value="/hello")
	public String sayHello() {
		return "Hey buddy Max !!!";
	}

}
