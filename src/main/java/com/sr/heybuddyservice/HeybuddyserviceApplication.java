package com.sr.heybuddyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeybuddyserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeybuddyserviceApplication.class, args);
	}

}
